"""
Скрипт для генерации промокодов в БД
"""
from pymongo import MongoClient
from main import PROMOCODE_NUMBER, MONGO_URI

client = MongoClient(MONGO_URI)

taxi = client.taxi

taxi.drop_collection('orders')
taxi.drop_collection('promocodes')

taxi.create_collection('promocodes')
# taxi.create_collection('orders')

# Создаём промокоды
for i in range(PROMOCODE_NUMBER):
    taxi.promocodes.insert({'in_use': False,
                            'uses': 0,
                            'orders': [],
                            'default_promocode': False})

# Создаём промокод по умолчания, к которому будут относиться заказы без промокода
taxi.promocodes.insert({'in_use': False,
                        'uses': 0,
                        'orders': [],
                        'default_promocode': True})

client.close()
