import requests
import unittest
from pymongo import MongoClient
from main import MAX_USES_PROMOCODE, MAX_USES_PROMOCODE_PER_USER, MONGO_URI

SUCCESS = 200
BAD_REQUEST = 400
ERROR = 500


class TestServerFunctionality(unittest.TestCase):
    def __init__(self, *a, **kw):
        super(TestServerFunctionality, self).__init__(*a, **kw)
        self.host = 'localhost:8080'
        self.command = 'orders'
        self.url = 'http://{}/{}'.format(self.host, self.command)
        self.max_uses = MAX_USES_PROMOCODE
        self.max_uses_per_user = MAX_USES_PROMOCODE_PER_USER
        self.client = MongoClient(MONGO_URI)

    def tearDown(self):
        self.client.close()

    def test_order_creation_get(self):
        """Тест на GET-запрос к странице создания заказа"""
        response = requests.get(self.url)
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_order_creation_post_without_promocode(self):
        """Тест создания заказа без промокода"""
        response = requests.post(self.url)
        self.assertEqual(response.status_code, SUCCESS)

    def test_order_info_correct(self):
        """Тест получения информации по корректному заказу"""
        response = requests.post(self.url)
        self.assertEqual(response.status_code, SUCCESS)
        url = '{}/{}'.format(self.url, response.text)
        response = requests.get(url)
        self.assertEqual(response.status_code, SUCCESS)

    def test_order_info_incorrect(self):
        """Тест получения информации по некорректному идентифкатору запроса"""
        url = '{}/{}'.format(self.url, 'qqqqq')
        response = requests.get(url)
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_order_success_close(self):
        """Тест успешного завершения заказа"""
        response = requests.post(self.url)
        self.assertEqual(response.status_code, SUCCESS)
        url = '{}/{}'.format(self.url, response.text)
        response = requests.post(url, data={'completion': 'success'})
        self.assertEqual(response.status_code, SUCCESS)

    def test_order_cancel_close(self):
        """Тест отмены заказа"""
        response = requests.post(self.url)
        self.assertEqual(response.status_code, SUCCESS)
        url = '{}/{}'.format(self.url, response.text)
        response = requests.post(url, data={'completion': 'cancel'})
        self.assertEqual(response.status_code, SUCCESS)

    def test_order_incorrect_params_close(self):
        """Тест передачи некорректных параметров в завршение заказа"""
        response = requests.post(self.url)
        self.assertEqual(response.status_code, SUCCESS)
        url = '{}/{}'.format(self.url, response.text)
        response = requests.post(url, data={'compl333etion': 'cancel'})
        self.assertEqual(response.status_code, BAD_REQUEST)
        response = requests.post(url, data={'completion': 'cancesssl'})
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_order_double_cancel_close(self):
        """Тест корректной обработки попытки дважды успешно завершить заказ"""
        response = requests.post(self.url)
        self.assertEqual(response.status_code, SUCCESS)
        url = '{}/{}'.format(self.url, response.text)
        response = requests.post(url, data={'completion': 'cancel'})
        self.assertEqual(response.status_code, SUCCESS)
        response = requests.post(url, data={'completion': 'cancel'})
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_order_double_success_close(self):
        """Тест корректной обработки попытки дважды отменить заказ"""
        response = requests.post(self.url)
        self.assertEqual(response.status_code, SUCCESS)
        url = '{}/{}'.format(self.url, response.text)
        response = requests.post(url, data={'completion': 'success'})
        self.assertEqual(response.status_code, SUCCESS)
        response = requests.post(url, data={'completion': 'success'})
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_order_with_promocode(self):
        """Тест создания заказа с промокодом"""
        promocode = self.client.taxi.promocodes.insert({'in_use': False,
                                                        'uses': 0,
                                                        'orders': [],
                                                        'default_promocode': False})
        response = requests.post(self.url, data={b'promocode': (str(promocode)).encode('utf-8'), b'user_id': b'aaa'})
        self.assertEqual(response.status_code, SUCCESS)

    def test_order_with_invalid_promocode(self):
        """Тест создания заказа с невалидным промокодом"""
        response = requests.post(self.url, data={b'promocode': b'aaa', b'user_id': b'aaa'})
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_promocode_global_limit_with_cancel(self):
        """Тест проверки глобального количества использований промокода"""
        id = ''
        user_id = ''
        promocode = self.client.taxi.promocodes.insert({'in_use': False,
                                                        'uses': 0,
                                                        'orders': [],
                                                        'default_promocode': False})
        for i in range(self.max_uses):
            user_id = str(i).encode('utf-8')
            response = requests.post(self.url,
                                     data={b'promocode': (str(promocode)).encode('utf-8'), b'user_id': user_id})
            id = response.text
            self.assertEqual(response.status_code, SUCCESS)

        response = requests.post(self.url,
                                 data={b'promocode': (str(promocode)).encode('utf-8'), b'user_id': b'testuser'})

        self.assertEqual(response.status_code, BAD_REQUEST)
        url = '{}/{}'.format(self.url, id)
        response = requests.post(url, data={'completion': 'cancel'})
        self.assertEqual(response.status_code, SUCCESS)

        response = requests.post(self.url,
                                 data={b'promocode': (str(promocode)).encode('utf-8'), b'user_id': user_id})
        self.assertEqual(response.status_code, SUCCESS)

    def test_promocode_user_limit_with_cancel(self):
        """Тест проверки корректной обработки количества использований промокода одним пользователем"""
        promocode = self.client.taxi.promocodes.insert({'in_use': False,
                                                        'uses': 0,
                                                        'orders': [],
                                                        'default_promocode': False})
        id = ''
        user_id = b'testuser'
        for i in range(self.max_uses_per_user):
            response = requests.post(self.url,
                                     data={b'promocode': (str(promocode)).encode('utf-8'), b'user_id': user_id})
            id = response.text
            self.assertEqual(response.status_code, SUCCESS)

        response = requests.post(self.url,
                                 data={b'promocode': (str(promocode)).encode('utf-8'), b'user_id': user_id})

        self.assertEqual(response.status_code, BAD_REQUEST)
        url = '{}/{}'.format(self.url, id)
        response = requests.post(url, data={'completion': 'cancel'})
        self.assertEqual(response.status_code, SUCCESS)

        response = requests.post(self.url,
                                 data={b'promocode': (str(promocode)).encode('utf-8'), b'user_id': user_id})
        self.assertEqual(response.status_code, SUCCESS)


if __name__ == '__main__':
    unittest.main(verbosity=2)
