from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET
from twisted.internet import reactor, defer
from twisted.web.resource import NoResource
from txmongo.connection import ConnectionPool
from bson.objectid import ObjectId

PROMOCODE_NUMBER = 5
MAX_USES_PROMOCODE = 10
MAX_USES_PROMOCODE_PER_USER = 3
MONGO_URI = 'mongodb://localhost:27017'


class TaxiServer(Resource):
    """
    Класс для генерации корневой страницы
    """

    def __init__(self, mongo_uri, promocode_number, max_uses, max_uses_per_user):
        """
            В конструкторе подключаемся к БД и задаём ресурсы по адресам '/' и '/orders'
        """
        Resource.__init__(self)
        mongo_connection = ConnectionPool(mongo_uri)
        self.putChild(b'', NoResource())
        self.putChild(b'orders', OrdersResource(mongo_connection, promocode_number, max_uses, max_uses_per_user))


class OrdersResource(Resource):
    """
        Класс для ответов на запросы по адресу '/orders'
    """

    def __init__(self, mongo_connection, promocode_number, max_uses, max_uses_per_user):
        """

        :param mongo_connection: Подключение к БД
        :param promocode_number: Количество промокодов
        :param max_uses: Максимальное количество использований промокода
        :param max_uses_per_user: Максимальное количество использовани промокода на одного человека
        """
        Resource.__init__(self)
        self.mongo_connection = mongo_connection
        self.max_uses = max_uses
        self.max_uses_per_user = max_uses_per_user
        self.promocode_number = promocode_number

    def getChild(self, path, request):
        """
        Добавлений обрабтчиков на страницы вида 'orders/<order_id>' и 'orders'

        :param path: Идентификатор заказа
        :param request: Запрос
        :return: Возвращает Resource, который генерирует ответы на запросы
        """
        # Если нужна корневая страница, то возвраем сам объект(будут запускаться его render)
        if not path:
            return self
        else:  # Ресурс для обработки идентификаторов заказа
            return Order(path, self.mongo_connection)

    def render_GET(self, request):
        """
        Возвращаем код ошибки 400, т. к. GET-метод не поддерживается
        :param request: Запрос
        :return:
        """
        request.setResponseCode(400)
        return b'Only POST request'

    def render_POST(self, request):
        """
        Асинхронная генерация ответа на POST-запрос. Запускает асинхронную функцию

        :param request: запрос
        :return:
        """
        self.create_order(request)
        return NOT_DONE_YET

    @defer.inlineCallbacks
    def create_order(self, request):
        """
        Функция создания заказа
        :param request:
        :return: При успешном создании заказа возвращает его идентификатор и код 200, иначе возвращает код ошибки
        """
        taxi = self.mongo_connection.taxi
        promocodes = taxi.promocodes
        # Если в параметрах есть идентификатор промокода и пользователя
        id = ObjectId()
        answer = (str(id)).encode('utf-8')
        if b'promocode' in request.args and b'user_id' in request.args:
            # Получаем параметры
            promocode = request.args[b'promocode'][0]
            user_id = request.args[b'user_id'][0]
            user_uses = '{0}_uses'.format(user_id.decode('utf-8'))
            if not ObjectId.is_valid(promocode.decode('utf-8')):
                request.setResponseCode(400)
                request.write(b'Invalid promocode')
                request.finish()
                return
            # Запрос к БД для поиска промокода.
            # При этом запрос проверяет, что пользователь не превысил
            # количество использований промокода и то, что не превышено
            # общее количество использований промокода
            find_query = {'_id': ObjectId(promocode.decode('utf-8')),
                          'uses': {'$lt': self.max_uses},
                          'default_promocode': False,
                          '$or': [{user_uses: {'$exists': False}},
                                  {user_uses: {'$exists': True, '$lt': self.max_uses_per_user}}
                                  ]
                          }
            # Селектор для обновления записей в БД.
            # Добавляет заказ и увеличивает счётчики использований промокода
            update_query = {'$inc': {'uses': 1, user_uses: 1},
                            '$push': {'orders': {'id': id,
                                                 'archive': False,
                                                 'user_id': user_id.decode('utf-8')
                                                 }}}

        else:  # Если промокода не было, то ищем специальный промокод по умолчанию и в него добавляем заказ
            find_query = {'default_promocode': True}
            update_query = {'$push': {'orders': {'id': id, 'archive': False}}}

        try:
            result = yield promocodes.find_and_modify(find_query, update_query)
        except Exception as e:
            request.setResponseCode(500)
            answer = b'Error'
        else:
            # Если запрос не вернул ни одного документа, то промокод достиг лимита использования
            if not result:
                request.setResponseCode(400)
                answer = b'Use promotional code prohibited'
            else:
                request.setResponseCode(200)
        request.write(answer)
        request.finish()


class Order(Resource):
    """
        Класс для генерации информации о заказе. Обрабатывает ссылки вида '/orders/<order_id>'
    """

    def __init__(self, order_id, mongo_connection):
        """

        :param order_id: Идентифкатор заказа
        :param mongo_connection: Подключение к БД
        """
        Resource.__init__(self)
        self.order_id = order_id
        self.mongo_connection = mongo_connection

    def render_GET(self, request):
        """
        Для GET-запроса запускается функция генерации информации о заказе
        :param request:
        :return:
        """

        self.get_order_from_db(request)
        return NOT_DONE_YET

    def render_POST(self, request):
        """
        Для POST-запроса запускается функция завершения заказа
        :param request:
        :return:
        """

        self.finish_order(request)
        return NOT_DONE_YET

    @defer.inlineCallbacks
    def finish_order(self, request):
        """
        Функция завершения заказа. Если пришли неправильные параметры запроса
        воозврщает HTTP-код 400

        :param request: запрос
        :return:
        """
        taxi = self.mongo_connection.taxi
        promocodes = taxi.promocodes
        # Проверка на корректность параметров
        if b'completion' not in request.args:
            request.setResponseCode(400)
            request.write(b'Incorrect parameter')
            request.finish()
            return
        else:
            # Проверка на то, что в completion параметре корректное значение
            completion = request.args[b'completion'][0]
            if completion not in (b'cancel', b'success'):
                request.setResponseCode(400)
                request.write(b'Incorrect parameter')
                request.finish()
                return
        # Проверка на валидность идентификатора
        if ObjectId.is_valid(self.order_id.decode('utf-8')):
            # Словарь для обновления коллекции промокодов
            find_query = {'orders.id': ObjectId(self.order_id.decode('utf-8'))}
            projection_query = {'orders.$': 1, 'default_promocode': True}
            # Ищем заказ по его идентификатору
            try:
                result = yield promocodes.find_one(find_query, fields=projection_query)
            except:
                request.setResponseCode(500)
                answer = b'Error'
            else:
                # Если такой заказ есть, то проверяем, использовался ли в нём промокод
                if result:
                    # Если заказ в архиве, то ничего не делаем
                    if result['orders'][0]['archive']:
                        answer = b'Order already closed'
                        request.setResponseCode(400)
                        request.write(answer)
                        request.finish()
                        return
                    else:
                        # Запрос для обновления заказа. Перевод его в архив и простановка флажка об отмене
                        update_query = {'$set': {'orders.$.archive': True, 'orders.$.cancel': completion == b'cancel'}}
                        if not result['default_promocode']:
                            # Если заказ отменён, то надо уменьшить количество использований промокодов
                            if completion == b'cancel':
                                update_query = {
                                    '$set': {'orders.$.archive': True, 'orders.$.cancel': completion == b'cancel'},
                                    '$inc': {'uses': -1, '{0}_uses'.format(result['orders'][0]['user_id']): -1}
                                }
                else:
                    request.setResponseCode(400)
                    answer = b'No such order'
                    request.write(answer)
                    request.finish()
                    return

                try:
                    # Инcтрукция обновления заказа. Помечаем заказ архивным и ставим флаг - отменён заказ или нет
                    result = yield promocodes.find_and_modify(find_query, update_query)
                except Exception:
                    request.setResponseCode(500)
                    answer = b'Error'
                else:
                    if result:
                        request.setResponseCode(200)
                        answer = b'OK'
                    else:  # Активного заказа с таким идентификатором не существует
                        request.setResponseCode(400)
                        answer = b'Incorrect id'
        else:  # Невалидный идентификатор
            request.setResponseCode(400)
            answer = b'Incorrect id'
        request.write(answer)
        request.finish()

    @defer.inlineCallbacks
    def get_order_from_db(self, request):
        """
            Функция получения информации о заказе
        :param request:
        :return:
        """
        taxi = self.mongo_connection.taxi
        promocodes = taxi.promocodes
        # Проверка на валидность присланного идентификатора
        if ObjectId.is_valid(self.order_id.decode('utf-8')):
            # Селектор заказа
            find_query = {'orders.id': ObjectId(self.order_id.decode('utf-8'))}
            # В ответе надо оставить информацию только о искомом заказе
            projection_query = {'orders.$': 1}
            try:
                result = yield promocodes.find_one(find_query, fields=projection_query)
            except Exception:
                request.setResponseCode(500)
                answer = 'Error'
            else:
                # Если в БД нет такого заказа, то возвращаем ошибку
                if not result:
                    answer = 'No such order'
                    request.setResponseCode(400)
                else:
                    answer = str(result)
        else:  # Невалидный идентификатор
            request.setResponseCode(400)
            answer = 'Incorrect id'
        request.write(answer.encode('utf-8'))
        request.finish()


if __name__ == '__main__':
    # Создаём корневой ресурс
    root = TaxiServer(MONGO_URI, PROMOCODE_NUMBER, MAX_USES_PROMOCODE, MAX_USES_PROMOCODE_PER_USER)

    factory = Site(root)

    # Запускаем реактор
    reactor.listenTCP(8080, factory)
    reactor.run()
